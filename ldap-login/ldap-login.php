<?php
/**
 * Plugin Name: LDAP Login
 * Plugin URI: https://bitbucket.org/smart4aviation/wordpress-ldap-login/wiki
 * Description: Handles LDAP login correctly.
 * Version: 0.1
 * Author: Jacek Kowalski
 * Author URI: http://jacekk.info/
 * License: BSD
 */

class LDAPLogin {
	const VERSION = '0.1';
	
	function __construct() {
		register_activation_hook(__FILE__, array($this, 'activate'));
		
		if(is_admin()) {
			add_action('admin_init', array($this, 'admin_init'));
			add_action('admin_menu', array($this, 'admin_menu'));
		}
		
		if(get_option('ldaplogin_enabled')) {
			add_filter('authenticate', array($this, 'authenticate'), 1, 3);
		}
	}
	
	// Activate plugin
	function activate() {
		switch(get_option('ldaplogin_version')) {
			default:
				add_option('ldaplogin_enabled', FALSE);
				
				add_option('ldaplogin_server', 'ldap://ldap.example.com:389/');
				add_option('ldaplogin_starttls', FALSE);
				add_option('ldaplogin_base_dn', 'ou=People,dc=example,dc=com');
				add_option('ldaplogin_filter', '');
				add_option('ldaplogin_level', 2);
				
				add_option('ldaplogin_bind_dn', '');
				add_option('ldaplogin_bind_pw', '');
				
				add_option('ldaplogin_attr_username', 'uid');
				add_option('ldaplogin_attr_firstname', 'givenName');
				add_option('ldaplogin_attr_lastname', 'sn');
				add_option('ldaplogin_attr_email', 'mail');
				
				add_option('ldaplogin_version', self::VERSION);
			break;
		}
	}
	
	function authenticate($user, $username, $password) {
		$username = stripslashes($username);
		$password = stripslashes($password);
		
		if($user instanceof WP_User || empty($username) || empty($password)) {
			return $user;
		}
		
		// Set connection string
		$ldap = @ldap_connect(get_option('ldaplogin_server'));
		if(!$ldap) {
			return new WP_Error(ldap_errno($ldap), 'LDAP connection error: '.ldap_error($ldap));
		}
		
		// Set basic options
		@ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
		@ldap_set_option($ldap, LDAP_OPT_REFERRALS, FALSE);
		@ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, 3);
		
		// STARTTLS if required
		if(get_option('ldaplogin_starttls')) {
			if(!@ldap_starttls($ldap)) {
				return new WP_Error(ldap_errno($ldap), 'LDAP STARTTLS error: '.ldap_error($ldap));
			}
		}
		
		// Bind as manager/anonymous
		$binddn = get_option('ldaplogin_bind_dn');
		if(!empty($binddn)) {
			if(!@ldap_bind($ldap, get_option('ldaplogin_bind_dn'), get_option('ldaplogin_bind_pw'))) {
				return new WP_Error(ldap_errno($ldap), 'LDAP manager bind error: '.ldap_error($ldap));
			}
		} else {
			if(!@ldap_bind($ldap)) {
				return new WP_Error(ldap_errno($ldap), 'LDAP anonymous bind error: '.ldap_error($ldap));
			}
		}
		
		// Sanitize username for LDAP search
		if(function_exists('ldap_escape')) {
			$username_tr = ldap_escape($username, '', LDAP_ESCAPE_FILTER);
		} else {
			$translate = array(
				'\\' => '\\5c',
				'*' => '\\2a',
				'(' => '\\28',
				')' => '\\29',
				"\0" => '\\00',
			);
			$username_tr = strtr($username, $translate);
		}
		
		// Search for user in LDAP
		$searchFilter = '(&'.get_option('ldaplogin_filter').'('.get_option('ldaplogin_attr_username').'='.$username_tr.'))';
		$attributesList = array_filter(array(get_option('ldaplogin_attr_username'), get_option('ldaplogin_attr_firstname'), get_option('ldaplogin_attr_lastname'), get_option('ldaplogin_attr_email')));
		$searchResult = FALSE;
		if(get_option('ldaplogin_depth') == 1) {
			$searchResult = @ldap_list($ldap, get_option('ldaplogin_base_dn'), $searchFilter, $attributesList);
		} else {
			$searchResult = @ldap_search($ldap, get_option('ldaplogin_base_dn'), $searchFilter, $attributesList);
		}
		if(!$searchResult) {
			return new WP_Error(ldap_errno($ldap), 'LDAP search error: '.ldap_error($ldap));
		}
		
		if(@ldap_count_entries($ldap, $searchResult) == 1) {
			$entry = @ldap_first_entry($ldap, $searchResult);
			if(!$entry) return new WP_Error(ldap_errno($ldap), 'LDAP search error: '.ldap_error($ldap));
			$dn = @ldap_get_dn($ldap, $entry);
			if(!$dn) return new WP_Error(ldap_errno($ldap), 'LDAP search error: '.ldap_error($ldap));
			$attrs = @ldap_get_attributes($ldap, $entry);
			if(!$attrs) return new WP_Error(ldap_errno($ldap), 'LDAP search error: '.ldap_error($ldap));
			
			
			@ldap_free_result($searchResult);
			
			if(!@ldap_bind($ldap, $dn, $password)) {
				 return new WP_Error(ldap_errno($ldap), 'LDAP bind error. Possibly invalid login and/or password.');
			}
			
			$username = NULL;
			if(get_option('ldaplogin_attr_username') && isset($attrs[get_option('ldaplogin_attr_username')][0])) {
				$username = $attrs[get_option('ldaplogin_attr_username')][0];
			}
			$firstname = NULL;
			if(get_option('ldaplogin_attr_firstname') && isset($attrs[get_option('ldaplogin_attr_firstname')][0])) {
				$firstname = $attrs[get_option('ldaplogin_attr_firstname')][0];
			}
			$lastname = NULL;
			if(get_option('ldaplogin_attr_lastname') && isset($attrs[get_option('ldaplogin_attr_lastname')][0])) {
				$lastname = $attrs[get_option('ldaplogin_attr_lastname')][0];
			}
			$email = NULL;
			if(get_option('ldaplogin_attr_email') && isset($attrs[get_option('ldaplogin_attr_email')][0])) {
				$email = $attrs[get_option('ldaplogin_attr_email')][0];
			}
			
			$user = get_user_by('login', $username);
			$user_data = array(
				'user_login'    => $username,
				'user_email'    => $email,
				'first_name'    => $firstname,
				'last_name'     => $lastname,
				'user_pass'     => '',
			);
			if(!$user) {
				$user_data = array_merge($user_data,
					array(
						'user_nicename' => ($firstname || $lastname ? trim($firstname.' '.$lastname) : $username),
						'nickname'      => ($firstname || $lastname ? trim($firstname.' '.$lastname) : $username),
						'display_name'  => ($firstname || $lastname ? trim($firstname.' '.$lastname) : $username),
					)
				);
				$result = wp_insert_user($user_data);
				var_dump($result);
				$user = get_user_by('id', $result);
			} else {
				$user_data['ID'] = $user->ID;
				wp_update_user($user_data);
			}
			
			return $user;
		} else {
			return null;
		}
		
		@ldap_free_result($searchResult);
		@ldap_unbind($ldap);
	}
	
	// Add entry in plugins menu
	function admin_menu() {
		// add_plugins_page( page_title, menu_title, permission, page-id, page_callback )
		add_options_page('LDAP Login - Plugin settings', 'LDAP Login', 'manage_options', 'ldap-login-page', array($this, 'adminPage'));
	}
	
	function add_action_links($links) {
		$links[] = '<a href="'. admin_url('options-general.php?page=ldap-login-page') .'">Settings</a>';
		return $links;
	}
	
	// Initialize admin panel hooks
	function admin_init() {
		add_filter('plugin_action_links_'.plugin_basename(__FILE__), array($this, 'add_action_links'));
		
		// add_settings_section( section-id, title, callback, page-id )
		add_settings_section('ldap-login-section', 'LDAP Login', NULL, 'ldap-login-page');
		
		// register_setting( opt-group , opt-name [, sanitizer] )
		// add_settings_field( id, title, callback, page-id, section-id [, args] )
		
		register_setting('ldap-login-group', 'ldaplogin_enabled', 'intval');
		add_settings_field('ldap-login-enabled', 'Status', array(__CLASS__, 'field_checkbox'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_enabled', 'title' => 'Plugin enabled'));
		
		register_setting('ldap-login-group', 'ldaplogin_server');
		add_settings_field('ldap-login-server', 'LDAP server url', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_server', 'description' => 'LDAP server URL, eg. <code>ldaps://ldap.example.com/</code>'));
		register_setting('ldap-login-group', 'ldaplogin_starttls', 'intval');
		add_settings_field('ldap-login-starttls', 'Encryption', array(__CLASS__, 'field_checkbox'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_starttls', 'title' => 'Use STARTTLS', 'description' => 'Uncheck when using ldaps:// connection scheme.'));
		
		register_setting('ldap-login-group', 'ldaplogin_bind_dn');
		add_settings_field('ldap-login-bind-dn', 'Bind DN', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_bind_dn', 'description' => 'Leave empty for anonymous bind'));
		register_setting('ldap-login-group', 'ldaplogin_bind_pw');
		add_settings_field('ldap-login-bind-pw', 'Bind password', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_bind_pw', 'description' => 'Leave empty for anonymous bind'));
		
		register_setting('ldap-login-group', 'ldaplogin_base_dn');
		add_settings_field('ldap-login-base-dn', 'Users base DN', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_base_dn', 'description' => 'Users search base DN, eg. <code>ou=People,dc=example,dc=com</code>'));
		register_setting('ldap-login-group', 'ldaplogin_depth', 'intval');
		add_settings_field('ldap-login-depth', 'Search depth', array(__CLASS__, 'field_special_depth'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_depth', 'description' => 'How deep to search under base DN'));
		register_setting('ldap-login-group', 'ldaplogin_filter');
		add_settings_field('ldap-login-filter', 'Users search filter', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_filter', 'description' => 'Users search filter, eg. <code>(objectClass=inetOrgPerson)</code>'));
		
		register_setting('ldap-login-group', 'ldaplogin_attr_username');
		add_settings_field('ldap-login-attr-username', 'Username attribute', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_attr_username', 'description' => 'Attribute containing user\'s login, eg. <code>uid</code>'));
		register_setting('ldap-login-group', 'ldaplogin_attr_firstname');
		add_settings_field('ldap-login-attr-firstname', 'First name attribute', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_attr_firstname', 'description' => 'Attribute containing user\'s first name, eg. <code>givenName</code>'));
		register_setting('ldap-login-group', 'ldaplogin_attr_lastname');
		add_settings_field('ldap-login-attr-lastname', 'Last name attribute', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_attr_lastname', 'description' => 'Attribute containing user\'s last name, eg. <code>sn</code>'));
		register_setting('ldap-login-group', 'ldaplogin_attr_email');
		add_settings_field('ldap-login-attr-email', 'E-mail address attribute', array(__CLASS__, 'field_text'), 'ldap-login-page', 'ldap-login-section',
			array('option' => 'ldaplogin_attr_email', 'description' => 'Attribute containing user\'s e-mail address, eg. <code>mail</code>'));
	}
	
	static function field_text($args) {
		$name = $args['option'];
		$value = get_option($name);
		
		echo '<input type="text" class="regular-text" id="'.esc_attr($name).'" name="'.esc_attr($name).'" value="'.esc_attr($value).'"/>'; 
		
		if(isset($args['description']))  {
			echo '<p class="description">'.$args['description'].'</p>';
		}
	}
	static function field_checkbox($args) {
		$name = $args['option'];
		$value = get_option($name);
		
		echo '<label><input type="checkbox" id="'.esc_attr($name).'" name="'.esc_attr($name).'" value="1" '.($value ? 'checked="checked"' : '').'/> '.$args['title'].'</label>'; 
		
		if(isset($args['description']))  {
			echo '<p class="description">'.$args['description'].'</p>';
		}
	}
	static function field_special_depth($args) {
		$name = $args['option'];
		$value = get_option($name);
		
		echo '<fieldset>'."\n"
			.'<label><input type="radio" id="'.esc_attr($name).'" name="'.esc_attr($name).'" value="1" '.($value == 1 ? 'checked="checked"' : '').'/> One level</label><br />'."\n"
			.'<label><input type="radio" id="'.esc_attr($name).'" name="'.esc_attr($name).'" value="2" '.($value != 1 ? 'checked="checked"' : '').'/> Whole subtree</label>'."\n"
			.'</fieldset>';
		
		if(isset($args['description']))  {
			echo '<p class="description">'.$args['description'].'</p>';
		}
	}
	
	// Display settings page
	function adminPage() {
		if(!current_user_can('manage_options')) {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}
		
		// Show options page
		echo '<div class="wrap">'."\n";
		echo '<h2>LDAP Login - Plugin settings</h2> '."\n";
		echo '<form method="post" action="options.php">'."\n";
		
		// settings_fields( opt-group )
		// do_settings_sections( page-id )
		settings_fields('ldap-login-group');
		do_settings_sections('ldap-login-page');
		submit_button();
		
		echo '</form>'."\n";
		echo '</div>'."\n";
	}
}

$LDAPLogin = new LDAPLogin();
